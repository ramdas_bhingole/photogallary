angular.module('photoGallery', ['ionic'])
.controller('ImageController', function($scope, $ionicModal, $state,$http,$window) {
  var pageNo = 1;

  $scope.loadMore = function() {
    console.log("Loading loadMore function pageNo is " + pageNo);
    $http.get('https://api.gettyimages.com/v3/search/images?page='+ (pageNo + 1) +'&page_size=2', {
        headers: {'Api-Key': 'ad25axusreknh38nzvw5szak'}})
    .success(function(data, status, headers, config){
        $scope.imageSizes = {};
        $scope.images = data.images;
        console.log("Data is "+ JSON.stringify(data));

        console.log("Data.images is " + JSON.stringify(data.images));
        console.log("data.images[0].display_sizes is " + JSON.stringify(data.images[1].display_sizes));

        $scope.spinnerFlag = false;
        console.log("Image uri is " + JSON.stringify($scope.imageSizes));

      })
    .error(function(holidays, status, headers, config) {
           console.log('GOT ERROR: ' + holidays);
      });
  };

  $scope.$on('$stateChangeSuccess', function() {
    $scope.loadMore();

  });

  $scope.spinnerFlag = true;

  $scope.showImgDetails = function(imageId){
    console.log("Hello clicked on image " + imageId );
    $state.go('details', {"id" : imageId} );
  };

});
