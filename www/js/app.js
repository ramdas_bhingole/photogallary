// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic'])

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider

  .state('images', {
    url: '/images',
    templateUrl: 'templates/images.html'
  })
  .state('details', {
      url: '/details/:id',
      templateUrl: 'templates/imageDetails.html',
      controller: 'DetailsCtrl',
      cache: false,
      reload: true
  });
  $urlRouterProvider.otherwise('/images');
})



.controller('MediaCtrl', function($scope, $ionicModal, $state,$http,$window) {
  var pageNo = 1;

  $scope.loadMore = function() {
    console.log("Loading loadMore function pageNo is " + pageNo);
    $http.get('https://api.gettyimages.com/v3/search/images?page='+ (pageNo + 1) +'&page_size=2', {
        headers: {'Api-Key': 'ad25axusreknh38nzvw5szak'}})
    .success(function(data, status, headers, config){
        $scope.imageSizes = {};
        $scope.images = data.images;
        console.log("Data is "+ JSON.stringify(data));

        console.log("Data.images is " + JSON.stringify(data.images));
        console.log("data.images[0].display_sizes is " + JSON.stringify(data.images[1].display_sizes));

        $scope.spinnerFlag = false;
        console.log("Image uri is " + JSON.stringify($scope.imageSizes));

      })
    .error(function(holidays, status, headers, config) {
           console.log('GOT ERROR: ' + holidays);
      });
  };

  $scope.$on('$stateChangeSuccess', function() {
    $scope.loadMore();

  });

  $scope.spinnerFlag = true;

  $scope.showImgDetails = function(imageId){
    console.log("Hello clicked on image " + imageId );
    $state.go('details', {"id" : imageId} );
  };

})

.controller('DetailsCtrl', function($scope,$state,$http,$stateParams) {

    $scope.spinnerFlag = true;
    console.log("Hello u r in detailsCtrl");
    console.log("Hello u r in detailsCtrl" + $stateParams.id + 'https://api.gettyimages.com/v3/images?ids='+ $stateParams.id +'&fields=detail_set,display_set');

    $http.get('https://api.gettyimages.com/v3/images?ids='+ $stateParams.id +'&fields=detail_set,display_set', {
      headers: {'Api-Key': 'ad25axusreknh38nzvw5szak'}
    }).
    success(function(data, status, headers, config){

      $scope.spinnerFlag = false;
      $scope.imageDetails = data;
      $scope.title= data.images[0].title;
      $scope.imgUrl = data.images[0].display_sizes[0].uri;
      console.log("************** "+ data.images[0].display_sizes[0].uri);
      console.log("Data is "+ JSON.stringify(data));

    }).
    error(function(holidays, status, headers, config) {
         console.log('GOT ERROR: ' + holidays);
    });

});

